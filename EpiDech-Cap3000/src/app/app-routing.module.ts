import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LikesComponent } from './components/loged/likes/likes.component';
import { ShopComponent } from './components/loged/shop/shop.component';
import { ShoppingCartComponent } from './components/loged/shopping-cart/shopping-cart.component';

import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { UserComponent } from './components/loged/user/user.component';

import { AuthGuard } from "./shared/guard/auth.guard";
import { ShopByIdComponent } from './components/loged/shop/shop-by-id/shop-by-id.component';
import { ReservationComponent } from './components/loged/shopping-cart/reservation/reservation.component';
import { LogedComponent } from './components/loged/loged.component';


/**
 * all routes of the project. if they are protected then canActivate: [AuthGuard].
 */
const routes: Routes = [
  { path: '', redirectTo: '/sign-in', pathMatch: 'full'},
  { path: 'sign-in', component: SignInComponent},
  { path: 'register-user', component: SignUpComponent},
  { path: 'menu', component: LogedComponent, canActivate: [AuthGuard]},
  { path: 'cart', component: ShoppingCartComponent, canActivate: [AuthGuard]},
  { path: 'subscribe', component: ReservationComponent, canActivate: [AuthGuard]},
  { path: 'shop', component: ShopComponent, canActivate: [AuthGuard]},
  { path: 'shop/view/:id', component: ShopByIdComponent, canActivate: [AuthGuard]},
  { path: 'likes', component: LikesComponent, canActivate: [AuthGuard]},
  { path: 'user', component: UserComponent, canActivate: [AuthGuard]}
];

UserComponent
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
