import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';

import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule  } from '@angular/fire/auth';
import { AngularFireModule } from "@angular/fire";

import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from "./shared/services/auth.service";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LogedComponent } from './components/loged/loged.component';
import { ShoppingCartComponent } from './components/loged/shopping-cart/shopping-cart.component';
import { LikesComponent } from './components/loged/likes/likes.component';
import { ShopComponent } from './components/loged/shop/shop.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { UserComponent } from './components/loged/user/user.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShopByIdComponent } from './components/loged/shop/shop-by-id/shop-by-id.component';
import { ReservationComponent } from './components/loged/shopping-cart/reservation/reservation.component';


@NgModule({
  declarations: [
    LogedComponent,
    AppComponent,
    SignInComponent,
    SignUpComponent,
    ShoppingCartComponent,
    LikesComponent,
    ShopComponent,
    SidebarComponent,
    UserComponent,
    ShopByIdComponent,
    ReservationComponent
  ],
  imports: [
    FontAwesomeModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [RouterModule],
  providers: [AuthService, ShopComponent],
  bootstrap: [AppComponent,]
})
export class AppModule { }
