import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { updateArrayBindingPattern, updateLanguageServiceSourceFile } from 'typescript';
import { ShopComponent } from '../shop/shop.component';

/**
 * page like a Product
 */
@Component({
  selector: 'app-likes',
  templateUrl: './likes.component.html',
  styleUrls: ['./likes.component.scss']
})


export class LikesComponent implements OnInit {
/**
 * table of Product
 */
  myarticle: any;

  /**
 * id of the actual Product printed
 */
  id: any;

  /**
 * user info
 */
  user: any;


  constructor( public router: Router, public authService: AuthService) { }

  ngOnInit(): void {
    this.authService.posts.subscribe();
    this.myarticle = this.authService.products;
    this.id = 0;
    this.authService.usrData.subscribe();
    this.user = this.authService.dataUsr;
  }

  
  checkbrand(i: number) {
    // console.log(this.myarticle[this.id].tags.brand);
    if (this.myarticle[this.id].tags.brand == "adidas") {
      this.user.tags.brand.adidas += i;
    }
    else if (this.myarticle[this.id].tags.brand == "banana moon") {
      this.user.tags.brand.bannana_moon += i;
    }
    else if (this.myarticle[this.id].tags.brand == "obey") {
      this.user.tags.brand.obey += i;
    }
    else if (this.myarticle[this.id].tags.brand == "pepejeans") {
      this.user.tags.brand.pepejeans += i;
    }
    else {
      return -1;
    }
    return 0;
  }

  checkcolor(i: number) {
    // console.log(this.myarticle[this.id].tags.color);
    if (this.myarticle[this.id].tags.color == "black") {
      this.user.tags.color.black += i;
    }
    else if (this.myarticle[this.id].tags.color == "white") {
      this.user.tags.color.white += i;
    }
    else if (this.myarticle[this.id].tags.color == "green") {
      this.user.tags.color.green += i;
    }
    else if (this.myarticle[this.id].tags.color == "red") {
      this.user.tags.color.red += i;
    }
    else {
      return -1;
    }
    return 0;
  }

 
  checktype(i: number) {
    // console.log(this.myarticle[this.id].tags.type);
    if (this.myarticle[this.id].tags.type == "dress") {
      this.user.tags.type.dress += i;
    }
    else if (this.myarticle[this.id].tags.type == "jacket") {
      this.user.tags.type.jacket += i;
    }
    else if (this.myarticle[this.id].tags.type == "pants") {
      this.user.tags.type.pants += i;
    }
    else if (this.myarticle[this.id].tags.type == "t_shirt") {
      this.user.tags.type.t_shirt += i;
    }
    else {
      return -1;
    }
    return 0;
  }


  checksubject(i: number) {
    //  console.log(this.myarticle[this.id].tags.subject);
    if (this.myarticle[this.id].tags.subject == "cotton") {
      this.user.tags.subject.cotton += i;
    }
    else if (this.myarticle[this.id].tags.subject == "lin") {
      this.user.tags.subject.lin += i;
    }
    else if (this.myarticle[this.id].tags.subject == "nylon") {
      this.user.tags.subject.nylon += i;
    }
    else if (this.myarticle[this.id].tags.subject == "polyester") {
      this.user.tags.subject.polyester += i;
    }
    else {
      return -1;
    }
    return 0;
  }

   /**
 * while the user like or dislike a product we check each tag of the article to add +1 or -1 at the same tag of the user
 * 
 * then whe modify the user in the database
 */
  addlike(i : number) {
    this.checkbrand(i);
    this.checksubject(i);
    this.checkcolor(i);
    this.checktype(i);
    // console.log("obey nbr : ", this.user.tags.brand.obey)
    this.authService.updateuserInfo(this.user);
  }

   /**
 * add a like or a dislike to the actual Product, then change the id pass to the next article. if the id of the next Product is 19 then the id is 0. (cause there is only 20 articles in the database)
 */
  preferencies(i: number) {
    if (this.id < 19) {
      this.addlike(i);
      this.id += 1;
    }
    else
      this.id = 0;
    // console.log(this.id);
  }


  /**
 * reset all préférencies of the user and so set all tags at 0
 */
  pref0() {
    this.user.tags.brand.obey = 0;
    this.user.tags.brand.bannana_moon = 0;
    this.user.tags.brand.adidas = 0;
    this.user.tags.brand.pepejeans = 0;
    this.user.tags.color.black = 0;
    this.user.tags.color.red = 0;
    this.user.tags.color.green = 0;
    this.user.tags.color.white = 0;
    this.user.tags.subject.lin = 0;
    this.user.tags.subject.cotton = 0;
    this.user.tags.subject.polyester = 0;
    this.user.tags.subject.nylon = 0;
    this.user.tags.type.dress = 0;
    this.user.tags.type.pants = 0;
    this.user.tags.type.t_shirt = 0;
    this.user.tags.type.jacket = 0;
    this.authService.updateuserInfo(this.user);
  }
}
