import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';

/**
 * Menu of the application
 */
@Component({
  selector: 'app-loged',
  templateUrl: './loged.component.html',
  styleUrls: ['./loged.component.scss']
})


export class LogedComponent implements OnInit {

  constructor(    public authService: AuthService,
    ) {
   }

   /**
 * table of Product
 */
   orderedPost!: any;

   /**
 * user info
 */
   user : any;

  ngOnInit(): void {
    this.get_postsdata();
    this.get_userdata();
  }

  /**
 * Get user info.
 */
  async get_userdata() {
    this.authService.usrData.subscribe();
    this.user = await this.authService.dataUsr;
  }
  
  /**
 * Get user table of product.
 */
  async get_postsdata() {
    this.authService.posts.subscribe();
    this.orderedPost = await this.authService.tab;
    // this.reOrderPosts();
  }
}
