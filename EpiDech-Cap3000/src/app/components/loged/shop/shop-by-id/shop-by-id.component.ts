import { Component, OnInit } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Cart } from 'src/app/shared/services/user';
import { ShopComponent } from '../shop.component';


/**
 * this page print the selected article in shop page;
 * 
 * in this page you can see more information about the Product, select a size, add it to your shopping cart
 */
@Component({
  selector: 'app-shop-by-id',
  templateUrl: './shop-by-id.component.html',
  styleUrls: ['./shop-by-id.component.scss']
})

export class ShopByIdComponent implements OnInit {
  
  /**
 * this Product
 */
  myarticle: any;

/**
 * this user info
 */
  user: any;

/**
 * the id of this Product
 */
  id: any;

  /**
 * name of the article
 */
  names: any;

  /**
 * size of the product selected by the user
 */
  size: any;

  constructor(private route: ActivatedRoute, public router: Router, public authService: AuthService) { }

  ngOnInit(): void {
    this.authService.posts.subscribe();
    const id = this.route.snapshot.params['id'] as number;
    this.myarticle = this.authService.products[id];
    this.myparse();
  }

    /**
 * get all size of the Product
 */
  async myparse() {
    this.names = this.myarticle.size.split(",", 4);
    this.size = [this.names[0]];
  }

/**
 * change the selected size of the Product by the user
 */
  dataChanged(name: string) {
    this.size = name;
  }

/**
 * add this product with this selected size to your shopping cart
 */
  add_to_cart() {
    this.user = this.authService.dataUsr;
    if (this.user.shop_cart[0] != undefined && this.user.shop_cart[0].id == 0) {
      this.user.shop_cart[0].id = this.myarticle.id;
      this.user.shop_cart[0].size = this.size;
    }
    else {
      const cart: Cart = { id: this.myarticle.id, size: this.size };
      this.user.shop_cart.push(cart);
    }
    this.authService.updateuserInfo(this.user);
    this.router.navigate(['/shop']);
  }

  onBack() {
    this.router.navigate(['/shop']);
  }
}
