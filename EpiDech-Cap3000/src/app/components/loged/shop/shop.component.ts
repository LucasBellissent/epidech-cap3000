import { Component, OnInit } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Product } from 'src/app/shared/services/product';
import { map } from 'rxjs/operators';

/**
 * page where all Product are print by preferencies of the user
 */
@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  public Products: Product[] | undefined;
  constructor(
    public authService: AuthService,
    private router: Router
  ) {
  }

/**
 * the max of like in a tags
 */
  max!: number;

/**
 * table of Product
 */
  orderedPost!: any;

   /**
 * user info
 */
  user: any;

  /**
 * tags mostly liked by the user
 */
  type: any;

  /**
 * child of tags mostly liked by the user
 */
  subtype: any;

  ngOnInit(): void {
    this.get_postsdata();
    this.get_userdata();
    // this.orderedPost.forEach((index: []) => {
    //   console.log(index);
    // })
  }

  /**
 * get dataUsr of Usr in user
 */
  async get_userdata() {
    this.authService.usrData.subscribe();
    this.user = this.authService.dataUsr;
  }

 /**
 * get tab of Product in orderedPost
 * 
 * get the préférencies of the user
 * 
 * re order the tab of Product by préférencies of the user
 */
  async get_postsdata() {
    this.authService.posts.subscribe();
    this.orderedPost = await this.authService.tab;
    this.getpreferencies();
    console.log(this.type, this.subtype);
    this.reOrderPosts(this.type, this.subtype);
  }
  //     this.authService.getAll().snapshotChanges().pipe(
  //       map(changes =>
  //         changes.map(c =>
  //           ({ id: c.payload.doc.id, ...c.payload.doc.data() })
  //         )
  //       )
  //     ).subscribe(data => {
  //       this.post1 = data;
  //     });
  //     // console.log(this.post1[1]);
  //     // for (let item of this.orderedPost) {
  //     //   this.post1 = item;
  //     //   //   if (item.data.id == id)
  //     //   //   this.post = item;
  //     //   // }    this.post1 = this.orderedPost;
  //     // }
  //   }


  /**
 * navigate to this article
 */
  onViewItem(id: string) {
    var numeric = Number(id);
    this.router.navigate(['/shop', 'view', numeric]);
  }

  //mySwap(indxo, indxt) {
  //  let temp = this.sac_a_dos[indxo];
  //
  //  this.sac_a_dos[indxo] = this.sac_a_dos[indxt];
  //  this.sac_a_dos[indxt] = temp;
  //},
  //
  //triSac() {
  //  for (let i = 0; i < this.sac_a_dos.length - 1; ++i) {
  //    let a = 0, b = 0;
  //
  //    while (!isNaN(this.sac_a_dos[i][a]))
  //      a++;
  //    while (!isNaN(this.sac_a_dos[i + 1][b]))
  //      b++;
  //    while (this.sac_a_dos[i][a] === this.sac_a_dos[i + 1][b]) {
  //      a++;
  //      b++;
  //    }
  //    if (this.sac_a_dos[i][a] > this.sac_a_dos[i + 1][b])
  //      this.mySwap(i, i + 1);
  //  }
  //}

  /**
 * get the mostly liked tags; then define subtype and type ; set max at this mostly liked tags value
 */
  isupper(value: number, type: string, subtype: string) {
    if (value > this.max) {
      this.subtype = subtype;
      this.type = type;
      this.max = value;
    }
  }

  /**
 * compare all préférencies of the user
 */
  getpreferencies() {
    this.type = "type";
    this.subtype = "t_shirt";
    this.max = 0;

    this.isupper(this.user.tags.brand.obey, 'brand', 'obey');
    this.isupper(this.user.tags.brand.bannana_moon, 'brand', 'banana moon');
    this.isupper(this.user.tags.brand.adidas, 'brand', 'adidas');
    this.isupper(this.user.tags.brand.pepejeans, 'brand', 'pepejeans');

    this.isupper(this.user.tags.color.black, 'color', 'black');
    this.isupper(this.user.tags.color.red, 'color', 'red');
    this.isupper(this.user.tags.color.green, 'color', 'green');
    this.isupper(this.user.tags.color.white, 'color', 'white');

    this.isupper(this.user.tags.type.dress, 'type', 'dress');
    this.isupper(this.user.tags.type.pants, 'type', 'pants');
    this.isupper(this.user.tags.type.t_shirt, 'type', 't_shirt');
    this.isupper(this.user.tags.type.jacket, 'type', 'jacket');

    this.isupper(this.user.tags.subject.lin, 'subject', 'lin');
    this.isupper(this.user.tags.subject.cotton, 'subject', 'cotton');
    this.isupper(this.user.tags.subject.polyester, 'subject', 'polyester');
    this.isupper(this.user.tags.subject.nylon, 'subject', 'nylon');


    this.reOrderPosts(this.type, this.subtype);
  }

  /**
 * swap to tab in the tab of Product
 */
  mySwap(indxo: number, indxt: number) {
    let temp = this.orderedPost[indxo];

    this.orderedPost[indxo] = this.orderedPost[indxt];
    this.orderedPost[indxt] = temp;
  }

  /**
 * reorder post by an given tag
 */
  reOrderPosts(reorderTag: string, value: string) {

    for (let item of this.orderedPost) {
      let a = 0
      switch (reorderTag) {
        case "brand":

          for (let i = 0; i < this.orderedPost.length - 1; ++i) {
            if (this.orderedPost[i].tags.brand === value) {
              this.mySwap(a, i);
              a++
            }
          }
          break;
        case "color":
          for (let i = 0; i < this.orderedPost.length - 1; ++i) {
            if (this.orderedPost[i].tags.color === value) {
              this.mySwap(a, i);
              a++
            }
          }
          break;
        case "subject":
          for (let i = 0; i < this.orderedPost.length - 1; ++i) {
            if (this.orderedPost[i].tags.subject === value) {
              this.mySwap(a, i);
              a++
            }
          }
          break;
        case "type":
          for (let i = 0; i < this.orderedPost.length - 1; ++i) {
            if (this.orderedPost[i].tags.type === value) {
              this.mySwap(a, i);
              a++
            }
          }
          break;
      }
    }
    // console.log("END")
    // for (let item of this.orderedPost) {
    //   console.log(item.name + " : " + item.tags.color)
    // }
  }
}
