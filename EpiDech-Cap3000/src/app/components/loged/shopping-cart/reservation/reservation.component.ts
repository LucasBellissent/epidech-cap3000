import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
 /**
 * fix an appointement for try your Product and get a confirmation 
 */
@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})


export class ReservationComponent implements OnInit {
  name = 'Angular 5';
  time = null;
  date = null;
  appointmentMessage = ""
  form: FormGroup;
  /**
 * this user info
 */
  user : any;

  constructor( private fb: FormBuilder , public authService: AuthService){

    this.user = this.authService.dataUsr;

    this.form = this.fb.group({
      'time' : [this.time, Validators.required],
      'date': [this.date, Validators.required]
    })
  }

  ngOnInit(): void {
  }
  getTime(){
    var res = this.form.value
    console.log(res)
    this.appointmentMessage = "Félicitations ! Votre réservation est validée pour le " + res.date.day + "/" + res.date.month + "/" + res.date.year + " à " + res.time + " pour 30 minutes." + " Une confirmation vous a été envoyée a l'adresse suivante: " + this.user.email 
  }
}
