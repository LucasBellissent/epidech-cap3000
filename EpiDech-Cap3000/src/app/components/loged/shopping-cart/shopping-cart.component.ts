import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';

/**
 * page of the ShoppingCart of the user.
 * 
 * in this page you can see this total price of your ShoppingCart but also modify it by deleting some articles or take an appointement for try articles
 */
@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})


export class ShoppingCartComponent implements OnInit {

  /**
   * user info
   */
  user: any;

  /**
 * table of Product
 */
  orderedPost!: any;

  /**
* total price
*/
  prix: any;

  constructor(private router: Router, public authService: AuthService) {
  }

  ngOnInit(): void {
    this.prix = 0;
    this.user = this.authService.dataUsr;
    this.orderedPost = this.authService.products;
    this.calcprice();
  }

  /**
 * add all price's article in the variable prix
 */
  calcprice() {
    if (this.user.shop_cart[0] != undefined)
      if (this.user.shop_cart[0].id != '') {
        for (let item of this.user.shop_cart) {
          this.prix += this.orderedPost[item.id].price as number;
        };
      }
  }

  go_to(str: string) {
    this.router.navigate([str]);
  }

  /**
 * delete this article from the user
 */
  delete_article(id: number) {
    this.user.shop_cart.splice(id, 1);
    this.authService.updateuserInfo(this.user);
  }

  /**
 * delete all article from the user
 */
  delete_allarticle() {
    this.user.shop_cart.splice(0);
    this.authService.updateuserInfo(this.user);
  }
}