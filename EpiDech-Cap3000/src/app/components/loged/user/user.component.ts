import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ShopComponent } from '../shop/shop.component';

/**
 * Component for testing if we are receive correctly the user info
 */
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  
  user : any;

  constructor(public authService: AuthService) {
   }

  ngOnInit(): void {
    this.authService.usrData.subscribe();
    this.user = this.authService.dataUsr;
  }

}
