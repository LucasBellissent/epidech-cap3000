import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
/**
 * the navigation in the site when you are auth
 */
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})



export class SidebarComponent implements OnInit {

  constructor(public authService: AuthService,  private router: Router
    ) {
   }
  ngOnInit(): void {
  }

  go_to(str: string) {
    this.router.navigate([str]);
  }
}
