import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from "../../shared/services/auth.service";

/**
 * sign in with email and password
 */
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})



export class SignInComponent implements OnInit {
  location: any;

  constructor(
    public authService: AuthService, 
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  sign() {
    // setTimeout(()=>{
    //   window.location.reload();
    // }, 5000);
    // this.router.navigate(['shop']); 
  }
}
