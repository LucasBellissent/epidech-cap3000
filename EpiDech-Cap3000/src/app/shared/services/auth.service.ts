import { Injectable, NgZone } from '@angular/core';
import { Cart, User, Usr } from "../services/user";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from "@angular/router";
import { Product } from './product';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { UserComponent } from 'src/app/components/loged/user/user.component';

/**
 * Services from firebase
 */
@Injectable({
  providedIn: 'root'
})

export class AuthService {
  userData: any; // Save logged in user data

  // user: User | undefined;
  item: Product | undefined;

  usrCol: AngularFirestoreCollection<Usr> | undefined;

  usrData: any;


  /**
 * My user
 */

  dataUsr: any;

  postsCol: AngularFirestoreCollection<Product> | undefined;
  posts: any;

  postDoc!: AngularFirestoreDocument<Product>;
  post!: Observable<Product | undefined>;
  firebase: any;

  // user: any;

  // user: any;
  usr!: Usr;

  /**
 * a table copy of tab from the database firebase(AngularFirestore) this one will not be modified
 */

  products: Product[] = [];


   /**
 * table of Product from the database firebase(AngularFirestore)
 */

  tab: Product[] = [];

  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,
    public ngZone: NgZone // NgZone service to remove outside scope warning
  ) {
    this.initProducts();
    /* Saving user data in localstorage when 
    logged in and setting up null when logged out */
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        this.getuserInfo(user);
        JSON.parse(localStorage.getItem('user') || '{}');
      } else {
        localStorage.setItem('user', 'null');
        JSON.parse(localStorage.getItem('user') || '{}');
      }
    })
  }

  /**
 * Sign in with email/password
 */

  async SignIn(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['menu']);
        });
        this.SetUserData(result.user);
        this.getuserInfo(result.user);
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  /**
 * Sign up with email/password
 */

  async SignUp(email: string, password: string) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        /* Call the SendVerificaitonMail() function when new user sign 
        up and returns promise */
        this.ngZone.run(() => {
          this.router.navigate(['menu']);
        });
        this.SetUserData(result.user);
        this.usr_init(result.user);
        this.getuserInfo(result.user);
        // this.ngZone.run(() => {
        //   this.router.navigate(['shop']);
        // });
        // this.router.navigate(['shop']);

        // for (var i = 0; i != 20; i++)
        // this.productData(0);
      }).catch((error) => {
        window.alert(error.message)
      })
  }

/**
 * Create a table of Product from the database firebase(AngularFirestore)
 */
  
  async initProducts() {
    // const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${uid}`);
    // // const item: AngularFirestoreDocument<any> = this.afs.doc(`/products/0`);
    //  var test =  this.afs.collection(`/products/${0}`).ref.get();


    // console.log( test);

    // console.log(userRef);
    var i = 0;
    this.postsCol = this.afs.collection('products');
    //this.posts = this.postsCol.valueChanges();
    this.posts = this.postsCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Product;
        const id = a.payload.doc.id;
        if (this.tab.length < 20) {
          data.id = i.toString();
          this.tab.push(data);
          this.products.push(data);
          i++;
        }
        return { data, id };
        // const name = a.payload.doc.name;
        // const size = a.payload.doc.size;
        // const price = a.payload.doc.price;
        // const url = a.payload.doc.url;
        // const urlImage = a.payload.doc.urlImage;
        // const tags = a.payload.doc.tags;
        // return { data, name, size, url, price, urlImage, tags };
      })
    }))

    // console.log(this.posts);

    // const snapshot = await this.afs.collection('products').get();
    // snapshot.forEach((doc) => {
    //   console.log(doc.id, '=>', doc.data());
    // });
    // return this.posts();
    // this.postDoc = this.afs.doc('posts/'+ 0);
    // this.post = this.postDoc.valueChanges();     
  }

/**
 * get the Usr who is auth from user in firestore
 */

  async getuserInfo(user: any) {
    this.usrCol = this.afs.collection('user');
    this.usrData = this.usrCol.snapshotChanges().pipe(map(action => {
      return action.map(a => {
        const data = a.payload.doc.data() as Usr;
        const id = a.payload.doc.id;
        if (user.uid === id) {
          this.dataUsr = data;
        };
        return { data, id };
      })
    }));
  }

/**
 * get the Usr who is auth from user in firestore
 */

  updateuserInfo(user: Usr) {
    // console.log(user);
    const userRef: AngularFirestoreDocument<Usr> = this.afs.doc(`user/${this.userData.uid}`);
    return userRef.set(user, {
      merge: true,
    })
  }

  //  getProduct(postId: string) {
  //   var collection = this.afs.collection("products/").doc(postId);
  //   return collection.snapshotChanges().pipe(map(action => {
  //     const data = action.payload.data() as Product;
  //     const id = action.payload.id;
  //     return { id, data };
  //   }));

  //   // return this.afs.doc('products/'+postId).valueChanges();

  //   // return usr.payload().data;
  //   // this.posts = usr.snapshotChanges().pipe(map(action => {
  //   //     const data = action.payload.data();
  //   //     const id = action.payload.id;
  //   //     return { data, id};
  //   //     // const name = a.payload.doc.name;
  //   //     // const size = a.payload.doc.size;
  //   //     // const price = a.payload.doc.price;
  //   //     // const url = a.payload.doc.url;
  //   //     // const urlImage = a.payload.doc.urlImage;
  //   //     // const tags = a.payload.doc.tags;
  //   //     // return { data, name, size, url, price, urlImage, tags };
  //   // }))
  //   // return new Promise(
  //   //   (resolve, reject) => {
  //   //     this.firebase.ref('/product/' +postId).once('value').then(
  //   //       (data: Product) => {
  //   //         resolve(data.val());
  //   //       }, (error: any) => {
  //   //         reject(error);
  //   //       }
  //   //     );
  //   //   }
  //   // );
  // }

  // getmyuser(uid: string) {
  //   var usr = this.afs.doc('users/'+uid);
  //   var myuser = usr.valueChanges();
  //   return myuser;
  // }

  // inituser() {
  //   var i = 0;
  //   this.postsCol = this.afs.collection('products');
  //   //this.posts = this.postsCol.valueChanges();
  //   this.posts = this.postsCol.snapshotChanges().pipe(map(actions => {
  //     return actions.map(a => {
  //       const data = a.payload.doc.data() as Product;
  //       const id = a.payload.doc.id;
  //       if(this.tab.length < 20) {
  //         data.id = i.toString();
  //         this.tab.push(data);
  //         i++;
  //       }
  //       return { data, id};
  //     })
  //   }))
  // }

/**
 * Create the Usr in the datatbase (in the table /user in firestore)
 */
  async usr_init(user: any) {
    const userRef: AngularFirestoreDocument<Usr> = this.afs.doc(`user/${user.uid}`);
    const cart: Cart = { id: '', size: '' };
    const userData: Usr = {
      uid: user.uid,
      email: user.email,
      genre: true,
      shop_cart: [cart],
      tags: {
        brand: {
          obey: 0,
          bannana_moon: 0,
          adidas: 0,
          pepejeans: 0,
        },
        color: {
          black: 0,
          red: 0,
          green: 0,
          white: 0,
        },
        subject: {
          lin: 0,
          cotton: 0,
          polyester: 0,
          nylon: 0,
        },
        type: {
          dress: 0,
          pants: 0,
          t_shirt: 0,
          jacket: 0,
        }
      }
    }
    return userRef.set(userData, {
      merge: true,
    })
  }
  
/**
 * Create the User in the datatbase (in the table /users in firestore)
 */
  async SetUserData(user: any) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email
    }
    // console.log(userRef);
    // this.router.navigate(['shop']);
    return userRef.set(userData, {
      merge: true,
    })
  }

  //   productData(i: number) {
  //     const userRef: AngularFirestoreDocument<any> = this.afs.doc(`products/${i}`);
  //     const userData: Product = {
  //       genre: '',
  //       name: '',
  //       size: '',
  //       price: 0,
  //       url: '',
  //       urlImage: '',
  //       tags: {
  //         brand: '',
  //         color: '',
  //         subject: '',
  //         type: ''
  //       },
  //     }
  //     return userRef.set(userData, {
  //       merge: true
  //     })
  // }

  // Send email verfificaiton when new user sign up
/**
 * Returns true when user is loged in and email is verified
 */
  get isLoggedIn(): boolean {
    // const user = JSON.parse(localStorage.getItem('user') || 'null');
    const user = JSON.parse(localStorage.getItem('user') || '{}');
    return (user !== {}) ? true : false;
  }

/**
 * logout the actual user
 */

  async SignOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');

      localStorage.clear();
      this.dataUsr = null;
      this.router.navigate(['sign-in']);
    })
  }
}