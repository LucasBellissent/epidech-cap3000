/**
 * interface of Products. /products in cloud firestore.
 */

export interface Product {
    id : string;
    val(): unknown;
    data : any;
    genre: string;
    name: string;
    size: string;
    price: number;
    url: string;
    urlImage: string;
    tags:  {
        brand: string;
        color: string;
        subject: string;
        type: string;
    };
 }