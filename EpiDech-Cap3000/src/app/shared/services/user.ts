/**
 * interface of user. /users in cloud firestore.
 */
export interface User {
    uid: string;
    email: string;
}

/**
 * interface of user. /user in cloud firestore.
 */
export interface Usr {
    uid: string;
    email: string;
    genre: boolean;
    shop_cart: [Cart];
    tags: {
        brand: {
            obey: number;
            bannana_moon: number;
            adidas: number;
            pepejeans: number;
        }
        color: {
            black: number;
            red: number;
            green: number;
            white: number;
        }
        subject: {
            lin: number;
            cotton: number;
            polyester: number;
            nylon: number;
        };
        type: {
            dress: number;
            pants: number;
            t_shirt: number;
            jacket: number;
        };
    }
}

export interface Cart {
    id: string,
    size: string
}