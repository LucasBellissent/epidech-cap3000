export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCgWFdPoKwJopBD7nxMaP4nQ7k-r6jCwjo",
    authDomain: "epidech-cap3000.firebaseapp.com",
    databaseURL: "https://epidech-cap3000.firebaseio.com",
    projectId: "epidech-cap3000",
    storageBucket: "epidech-cap3000.appspot.com",
    messagingSenderId: "194856254243",
    appId: "1:194856254243:web:f4548ceaee0b8f36aa2a96",
    measurementId: "G-8KW6QG4LB3"
  }
};
