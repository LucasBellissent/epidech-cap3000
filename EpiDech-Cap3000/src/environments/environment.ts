// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


 /**
 * firebase env
 */
export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCgWFdPoKwJopBD7nxMaP4nQ7k-r6jCwjo",
    authDomain: "epidech-cap3000.firebaseapp.com",
    databaseURL: "https://epidech-cap3000.firebaseio.com",
    projectId: "epidech-cap3000",
    storageBucket: "epidech-cap3000.appspot.com",
    messagingSenderId: "194856254243",
    appId: "1:194856254243:web:f4548ceaee0b8f36aa2a96",
    measurementId: "G-8KW6QG4LB3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
